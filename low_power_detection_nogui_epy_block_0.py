"""
Embedded Python Blocks:

Each time this file is saved, GRC will instantiate the first class it finds
to get ports and parameters of your block. The arguments to __init__  will
be the parameters. All of them are required to have default values!
"""

import numpy as np
from gnuradio import gr
import time
import pmt

class blk(gr.sync_block):  # other base classes are basic_block, decim_block, interp_block
    """
    This embedded python block inserts a tag with a timestamp in
    epoch time in nanosecond format

    the tag spacing can be done with 
    
    """

    def __init__(self, tag_interval = 1000000):  # only default arguments here
        """arguments to this function show up as parameters in GRC"""
        gr.sync_block.__init__(
            self,
            name='Timestamp Adder',   # will show up in GRC
            in_sig=[np.complex64],
            out_sig=[np.complex64]
        )
        # if an attribute with the same name as a parameter is found,
        # a callback is registered (properties work, too).
        self.tag_interval = tag_interval
        self.offset = self.tag_interval

    def work(self, input_items, output_items):
        """example: multiply with constant"""
        output_items[0][:] = input_items[0]
        
        #write tag
        key = pmt.intern("timestamp")
        value = pmt.from_long(int(time.time_ns() // 1000000)) #epoch time in ms


        if(self.offset < self.nitems_written(0)):
            self.add_item_tag(0, # Write to output port 0
                    self.nitems_written(0) + self.tag_interval, # Index of the tag in absolute terms
                    key, # Key of the tag
                    value # Value of the tag
            )
            self.offset = self.offset + self.tag_interval
        
        return len(output_items[0])
    

