"""
Embedded Python Blocks:

Each time this file is saved, GRC will instantiate the first class it finds
to get ports and parameters of your block. The arguments to __init__  will
be the parameters. All of them are required to have default values!
"""

import numpy as np
from gnuradio import gr
import csv
import pmt
from datetime import datetime
import os
import pandas as pd
from scipy import interpolate


class blk(gr.sync_block):  # other base classes are basic_block, decim_block, interp_block
    """
    This block looks for tags with tag value "timestamp" and then 
    pairs the tag offset with the same having the same offset in 
    the input stream.

    The tag value (the timestamp itself) and the sample corresponding
    to a peak power is then paired up and saved in .csv format compatible
    with FANG. 

    [file_prefix]: specify a prefix in the name of the csv file. 
    .csv extension and date added automatically, so do not enter.

    csv file will be saved in the same folder as the flowgraph is
    executed.

    
    """

    def __init__(self, 
                 file_prefix='prefix_'):  # only default arguments here
        """arguments to this function show up as parameters in GRC"""
        gr.sync_block.__init__(
            self,
            name='FANG CSV saver',   # will show up in GRC
            in_sig=[np.float32],
            out_sig=None
            
        )
        # if an attribute with the same name as a parameter is found,
        # a callback is registered (properties work, too).
        
        self.file_prefix = file_prefix
        self.save_path = ""


        self.nsweep = 0
        self.first_instance = True


        
    def work(self, input_items, output_items):
        """example: multiply with constant"""

        #look for avaiable tags and put them in tags variable
        tags = self.get_tags_in_window(0, 0, len(input_items[0]))



        #Check if we need to create a new file
        if(self.first_instance):

            #Generate new timestamp
            current_datetime = datetime.now()
            self.save_path = self.file_prefix + current_datetime.strftime("%d_%m_%Y_%H_%M_%S") + ".csv"


            #Create a new file
            csv_file = open(self.save_path,'w')
            writer = csv.writer(csv_file)

            for i in range(0,11):
                writer.writerow(['#comment'])


            writer.writerow(['Sweep','Timestamp','Peak Freq MHz','Peak Ampl dBM'])    
            csv_file.close()

            #Reset sweep numbers
            self.nsweep = 0

            #Set first instance to false in order to avoid creating a new file
            self.first_instance = False

            
    
        #Check if there are tags
        if(tags):

            keys = []
            values = []
            row_to_write = [0,0,0,0]
            found_all_tags = 0
            
            #convert tags from PMT to python readable format
            for tag in tags:
                row_to_write[0] = self.nsweep   #sweep number
                row_to_write[1] = tag.value     #timestamp
                row_to_write[2] = 501           #frequency at peak power (DUMMY)
                row_to_write[3] = input_items[0][tag.offset - self.nitems_read(0)] #Peak power. Arbitrary value.
                
    
            #write data
            self.nsweep = self.nsweep + 1
            with open(self.save_path, 'a') as f_object:
                writer_object = csv.writer(f_object)
                writer_object.writerow(row_to_write)

        tags = []
            
        

        return len(input_items[0])
