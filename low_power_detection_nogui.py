#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: low_power_detection_nogui
# GNU Radio version: 3.10.1.1

from gnuradio import analog
from gnuradio import blocks
from gnuradio import fft
from gnuradio.fft import window
from gnuradio import filter
from gnuradio.filter import firdes
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import low_power_detection_nogui_epy_block_0 as epy_block_0  # embedded python block
import low_power_detection_nogui_epy_block_0_0 as epy_block_0_0  # embedded python block




class low_power_detection_nogui(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "low_power_detection_nogui", catch_exceptions=True)

        ##################################################
        # Variables
        ##################################################
        self.vector_length = vector_length = 2048
        self.samp_rate = samp_rate = 5e6
        self.decimation = decimation = 200

        ##################################################
        # Blocks
        ##################################################
        self.freq_xlating_fir_filter_xxx_0 = filter.freq_xlating_fir_filter_ccc(decimation, firdes.complex_band_pass(1, samp_rate, -samp_rate/(4*decimation), samp_rate/(4*decimation),10000), 1e6, samp_rate)
        self.fft_vxx_0 = fft.fft_vcc(vector_length, True, window.flattop(vector_length), True, 1)
        self.epy_block_0_0 = epy_block_0_0.blk(file_prefix='uplink_')
        self.epy_block_0 = epy_block_0.blk(tag_interval=int(5000000))
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate,True)
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_gr_complex*1, vector_length)
        self.blocks_nlog10_ff_0 = blocks.nlog10_ff(1, vector_length, 0)
        self.blocks_moving_average_xx_0 = blocks.moving_average_ff(int(10), 1/10, 4000, vector_length)
        self.blocks_max_xx_0 = blocks.max_ff(vector_length, 1)
        self.blocks_complex_to_mag_0 = blocks.complex_to_mag(vector_length)
        self.blocks_add_xx_0 = blocks.add_vcc(1)
        self.analog_sig_source_x_0 = analog.sig_source_c(samp_rate, analog.GR_COS_WAVE, 1e6, 1, 0, 0)
        self.analog_fastnoise_source_x_0 = analog.fastnoise_source_c(analog.GR_GAUSSIAN, 1, 0, 8192)


        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_fastnoise_source_x_0, 0), (self.blocks_add_xx_0, 0))
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_add_xx_0, 1))
        self.connect((self.blocks_add_xx_0, 0), (self.blocks_throttle_0, 0))
        self.connect((self.blocks_complex_to_mag_0, 0), (self.blocks_nlog10_ff_0, 0))
        self.connect((self.blocks_max_xx_0, 0), (self.epy_block_0_0, 0))
        self.connect((self.blocks_moving_average_xx_0, 0), (self.blocks_max_xx_0, 0))
        self.connect((self.blocks_nlog10_ff_0, 0), (self.blocks_moving_average_xx_0, 0))
        self.connect((self.blocks_stream_to_vector_0, 0), (self.fft_vxx_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.epy_block_0, 0))
        self.connect((self.epy_block_0, 0), (self.freq_xlating_fir_filter_xxx_0, 0))
        self.connect((self.fft_vxx_0, 0), (self.blocks_complex_to_mag_0, 0))
        self.connect((self.freq_xlating_fir_filter_xxx_0, 0), (self.blocks_stream_to_vector_0, 0))


    def get_vector_length(self):
        return self.vector_length

    def set_vector_length(self, vector_length):
        self.vector_length = vector_length

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)
        self.freq_xlating_fir_filter_xxx_0.set_taps(firdes.complex_band_pass(1, self.samp_rate, -self.samp_rate/(4*self.decimation), self.samp_rate/(4*self.decimation),10000))

    def get_decimation(self):
        return self.decimation

    def set_decimation(self, decimation):
        self.decimation = decimation
        self.freq_xlating_fir_filter_xxx_0.set_taps(firdes.complex_band_pass(1, self.samp_rate, -self.samp_rate/(4*self.decimation), self.samp_rate/(4*self.decimation),10000))




def main(top_block_cls=low_power_detection_nogui, options=None):
    tb = top_block_cls()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    try:
        input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
